#include "CipherLib/Trivium.h"
#include "CipherLib/Bivium.h"
#include "CipherLib/Threshold.h"
#include "CipherLib/Summative.h"
#include "CipherLib/Gifford.h"

#include "Common/DimacsParser.h"
#include "Common/CharStream.h"
#include "Common/Utils.h"
#include "Common/RndGen.h"
#include "Common/OutputFunctions.h"

#include <boost/shared_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/program_options.hpp>

#include <fstream>

typedef boost::shared_ptr<CipherLib::IStreamCipher> StreamCipherPtr;

int main(int argc, char* argv[])
{
	try
	{
		namespace po = boost::program_options;
		po::options_description options("Usage: CipherApp [options]");
		options.add_options()
			("help,h", "display this help and exit")
			("version,v", "output current program version")
			("rnd,r", po::value<std::string>()->default_value("true_random"), "get file with true random sequence")
			("cipher,c", po::value<std::string>(), "get cipher code (required)")
			("input,i", po::value<std::string>(), "get input template file name (required)")
			("length,l", po::value<std::string>(), "get stream length (required)")
			("test-cnt,t", po::value<std::string>()->default_value("10"), "get test count") 
			;

		po::variables_map params;
		po::store(po::parse_command_line(argc, argv, options), params);

		if(params.count("help"))
		{
			std::cout << options;
			return 0;
		}
		if(params.count("version"))
		{
			std::cout << "Build date: " << __DATE__ << " " << __TIME__ << std::endl;
			return 0;
		}

		//! ����, ������� ���������� �������� ����� (������������ ��������) 
		if(!params.count("cipher"))
			throw std::invalid_argument("cipher is not specified (option: -c, --cipher).");
		
		//! ���� � ��������� ��� (������������ ��������)
		if(!params.count("input"))
			throw std::invalid_argument("input file (template) is not specified (option: -i, --input).");

		//! ����� ��������� ������ (������������ ��������)
		if(!params.count("length"))
			throw std::invalid_argument("stream length is not specified (option: -l, --length).");

		const std::string cipher = params["cipher"].as<std::string>();
		const std::string in_file = params["input"].as<std::string>();
		const std::string rnd_file = params["rnd"].as<std::string>();
		const std::size_t stream_len = params["length"].as<std::size_t>();
		const std::size_t test_cnt = params["test-cnt"].as<std::size_t>();

		SatLib::Problem cnf;
		std::vector<bool> is_xor;
		{
			std::ifstream file(in_file.c_str(), std::ios::in);
			if(!file.is_open())
				throw std::runtime_error("can't open file " + in_file);
			SatLib::CharStream ss(file);
			SatLib::ParseDimacs(ss, cnf, is_xor);
		}

		SatLib::VarId min_var_id(0), max_var_id(0);
		SatLib::GetMinMaxVars(cnf, min_var_id, max_var_id);

		//! ������� ��������� ����������
		StreamCipherPtr sc;
		if(cipher == "bivium")
			sc.reset(new CipherLib::Bivium());
		else if(cipher == "trivium")
			sc.reset(new CipherLib::Trivium());
	
		if(!sc.get())
			throw std::runtime_error("stream generator is not created.");
	
		SatLib::GenTrueRandom rnd(rnd_file);

		for(std::size_t test_idx(1); test_idx <= test_cnt; ++test_idx)
		{
			std::cout << "generate test #" << test_idx << std::endl;
			// ��������� ���� � ����. ������ ����� �� ����� true_random
			std::vector<char> key, iv;	
			rnd.GetRandomStream(80, key);
			rnd.GetRandomStream(80, iv);


			SatLib::BitVector reg_state; 
			SatLib::BitVector stream_bits;
			
			// to do: ������ reset, setKey, setIV ����� ���������� � ���� �����
			sc->reset();
			sc->setKey(key);
			sc->setIV(iv);

			sc->init();

			sc->getRegisterState(reg_state);
			sc->getStreamBit(stream_bits, stream_len);

			//! ����
			{
				const std::string filename = cipher + std::string("_test_") + 
					boost::lexical_cast<std::string>(test_idx) + std::string(".cnf");
				std::ofstream out(filename.c_str(), std::ios::out);
				if(!out.is_open())
					throw std::runtime_error("can't open file " + filename);
				out << "p cnf " << max_var_id << " " << cnf.size() + stream_bits.size() << std::endl;
				out << "c input variables " << reg_state.size() << std::endl;
				//! ������
				for(std::size_t i(0); i < cnf.size(); ++i)
				{
					out << (is_xor[i] ? "x":"");
					const SatLib::Clause& cl = cnf[i];
					for(std::size_t j(0); j < cl.size(); ++j)
						out << (cnf[i][j]&1 ? "-" : "") << (cnf[i][j] >> 1) << " ";
					out << "0\n";
				}
				//! �������� �����
				std::size_t var_id = max_var_id - stream_bits.size() + 1;
				for(std::size_t i(0); i < stream_bits.size(); ++i, ++var_id)
				{
					out << (stream_bits[i] ? "" : "-") << var_id << " 0\n";
				}
			}
			//! ����
			if(key.size() || iv.size())
			{
				const std::string filename = cipher + std::string("_key_") + boost::lexical_cast<std::string>(test_idx);
				std::ofstream out(filename.c_str(), std::ios::out);
				if(!out.is_open())
					throw std::runtime_error("can't open file " + filename);
				if(key.size())
				{
					out << "Key:" << std::endl;
					SatLib::WriteBitVectorValues(out, key, 0);
				}
				if(iv.size())
				{
					out << std::endl << "Initial values:" << std::endl;
					SatLib::WriteBitVectorValues(out, iv, 0);
				}
			}
			//! ��������� ��������� ����� ���� ������������� (� dimacs-�������)
			{
				const std::string filename = cipher + std::string("_reg_") + boost::lexical_cast<std::string>(test_idx);
				SatLib::WriteBitVectorValues(filename, reg_state, 1, " 0\n");
			}
			//! �������� �����
			{
				const std::string filename = cipher + std::string("_stream_") + boost::lexical_cast<std::string>(test_idx);
				const unsigned var_id = max_var_id - stream_bits.size() + 1;
				SatLib::WriteBitVectorValues(filename, stream_bits, var_id, " 0\n");
			}
		}
	}
	catch(const std::exception& e)
	{
		std::cerr << "[ERROR]: " << e.what() << std::endl;
		return 1;
	}

	return 0;
}
