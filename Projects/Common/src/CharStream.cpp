﻿#include "../CharStream.h"

namespace SatLib
{

CharStream::CharStream(std::istream& s)
	: in(s)
	, pos_(0)
	, size_(0)
{
	buffer_ = new char[buffer_size];
	AssureLookahead();
}

CharStream::~CharStream()
{
	delete[] buffer_;
}

void CharStream::AssureLookahead()
{
	if(pos_ >= size_)
	{
		in.read(buffer_, buffer_size);
		size_ = static_cast<unsigned>(in.gcount());
		pos_ = 0;
	}
}

} // namespace SatLib
