#include "../RndGen.h"
#include <cstdlib>
#include <ctime>

namespace SatLib
{

bool GetRandomBit()
{
	short int v = rand();
    v ^= v >> 8;
    v ^= v >> 4;
    v ^= v >> 2;
    v ^= v >> 1;
    return static_cast<bool>(v & 1);
}

uint8_t GetRandomByte()
{
	short int v = rand();
    v ^= v >> 8;
    return static_cast<uint8_t>(v & 0xFF);
}

GenTrueRandom::GenTrueRandom(const std::string& filename)
	: file_(filename.c_str(), std::ios::binary)
	, file_size_(0)
{
	if(!file_.is_open())
		throw std::runtime_error(std::string("GenTrueRandom: can't open file ") + filename);
	//! ���������� ������ �����
	file_.seekg(0, std::ios::end);
	file_size_ = (std::size_t)file_.tellg();
	file_.seekg(0, std::ios::beg);
	//! �������������� ��������� ��������� ����� ������� ��������� ��������
	srand((unsigned int)time(NULL));
}

bool GenTrueRandom::GetRandomStream(std::size_t size, std::vector<char>& stream)
{
	const std::size_t read_size = size / 8 + (size % 8 ? 1 : 0);
	if(read_size > file_size_)
		return false;

	//! ������������� ������ �� ��������� ������� � �����
	const uint16_t pos = GetRandom<uint16_t>() % (file_size_ - read_size);
	file_.seekg(pos, std::ios::beg);

	//! ������ ������ �� �����
	std::vector<char> buf(read_size);
	file_.read(&buf[0], read_size);
	stream.resize(size);
	for(std::size_t i(0); i < size; ++i)
	{
		stream[i] = buf[i/8] & (1 << (i%8));
	}
	return true;
}

bool GenTrueRandom::GetRandomBytes(std::size_t size, std::vector<uint8_t>& stream)
{
	if(size > file_size_)
		return false;

	//! ������������� ������ �� ��������� ������� � �����
	const uint16_t pos = GetRandom<uint16_t>() % (file_size_ - size);
	file_.seekg(pos, std::ios::beg);

	//! ������ ������ �� �����
	stream.resize(size);
	file_.read(reinterpret_cast<char*>(&stream[0]), size);
	return true;
}

} // namespace SatLib
