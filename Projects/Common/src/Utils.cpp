#include "../Utils.h"
#include <iostream>
#include <fstream>
#include <string>

namespace SatLib
{

//! ��������� ������������� �����
bool FileExists(const std::string& filename)
{
	std::fstream fin(filename.c_str(), std::ios::in);
	if(fin.is_open())
	{
		fin.close();
		return true;
	}
	return false;
}

//! ������� ��������� ���������� ����������
void ExtractVarSet(const Problem& problem, VarSet& var_set)
{
	var_set.clear();
	for(Problem::const_iterator it = problem.begin(); it != problem.end(); ++it)
	{
		const Clause& c = *it;
		for(Clause::const_iterator cl_it = c.begin(); cl_it != c.end(); ++cl_it)
		{
			const Lit& lit = *cl_it;
			var_set.insert(static_cast<VarId>(lit >> 1));
		}
	}
}

void ExtractVarCounterMap(const Problem& problem, VarCounterMap& var_map)
{
	var_map.clear();
	for(Problem::const_iterator it = problem.begin(); it != problem.end(); ++it)
	{
		const Clause& c = *it;
		for(Clause::const_iterator cl_it = c.begin(); cl_it != c.end(); ++cl_it)
		{
			const VarId var_id = *cl_it >> 1;
			VarCounterMap::iterator it = var_map.find(var_id);
			if(it == var_map.end())
			{
				var_map.insert(VarCounter(var_id, 1));
			}
			else
			{
				it->second++;
			}
		}
	}
}

//! ����������� � ������������ ����� ����������
void GetMinMaxVars(const Problem& problem, VarId& min_var_id, VarId& max_var_id)
{
	VarSet var_set;
	ExtractVarSet(problem, var_set);
	min_var_id = *(var_set.begin());
	max_var_id = *(var_set.rbegin());
}

void SplitFilename(const std::string& path, std::string& dir, std::string& filename)
{
	const unsigned slash_pos = path.find_last_of("/\\");
	if(slash_pos != std::string::npos)
	{
		const unsigned dir_len = slash_pos + 1;
		dir = path.substr(0, dir_len);
		filename = (path.size() > dir_len ? path.substr(dir_len) : "");
	}
	else
	{
		dir.clear();
		filename = path;
	}
}

void SplitFileSuffix(const std::string& filename, std::string& name, std::string& suffix)
{
	const unsigned dot_pos = filename.find_last_of('.');
	if(dot_pos != std::string::npos)
	{
		name = filename.substr(0, dot_pos);
		suffix = filename.substr(dot_pos);
	}
	else
	{
		name = filename;
		suffix.clear();
	}
}

size_t GetLiteralCount(const Problem& problem)
{
	size_t cnt = 0;
	for(unsigned i = 0; i < problem.size(); ++i)
	{
		cnt += problem[i].size();
	}
	return cnt;
}

//! ������ ������� � ������
size_t GetProblemBinarySize(const Problem& problem)
{
	return sizeof(Lit) * GetLiteralCount(problem);
}

//! ������ ���������� ������ ��� �������
size_t GetProblemMemoryAlloc(const Problem& problem)
{
	size_t capacity_size = 0;
	for(unsigned i = 0; i < problem.size(); ++i)
	{
		capacity_size += problem[i].capacity();
	}
	// to do: ��� �������� ������, ���������� ��� ������, ���������� ��� �������?
	return capacity_size * sizeof(Lit);
}

void FillVarOrder(const Problem& problem, VarOrder& order, const std::string& order_type)
{
	order.clear();
	//! ������� ���������� ������������ ���������� �� ����������� �� �������
	if(order_type == "direct")
	{
		//! �������� ��������� ������� ����������, ������������� �� �����������
		VarSet var_set;
		ExtractVarSet(problem, var_set);
		//! ������������ ����� ����������
		const VarId max_var_id = *(var_set.rbegin());
		order.resize(static_cast<size_t>(max_var_id) + 1, 0);
		unsigned index = 0;
		for(VarSet::const_iterator it = var_set.begin(); it != var_set.end(); ++it)
		{
			const VarId var_id = *it;
			order[var_id] = ++index;
		}
	}
	//! ������� ���������� ������������ �������� ���������� �� ��������� � �������
	else if(order_type == "frequence")
	{
		VarCounterMap var_map;
		ExtractVarCounterMap(problem, var_map);

		const VarId max_var_id = var_map.rbegin()->first;
		order.resize(static_cast<size_t>(max_var_id) + 1, 0);
		for(VarCounterMap::const_iterator it = var_map.begin();
			it != var_map.end();
			++it)
		{
			order[it->first] = it->second;
		}
	}
	else
	{
		throw std::invalid_argument("FillVarOrder: Invalid order type argument: '" + order_type + "'.");
	}
}

/*ProblemType GetProblemType(const std::string& str_type)
{
	static std::map<std::string, ProblemType> problem_type_map;
	if(problem_type_map.empty())
	{
		problem_type_map.insert(std::make_pair("conflict", ProblemTypeConflict));
		problem_type_map.insert(std::make_pair("cnf", ProblemTypeCnf));
		problem_type_map.insert(std::make_pair("dnf", ProblemTypeDnf));
	}
	std::map<std::string, ProblemType>::const_iterator it = problem_type_map.find(str_type);
	if(it == problem_type_map.end())
	{
		throw std::invalid_argument("GetProblemType: invalid problem type!");
	}
	return it->second;
}*/

void NegateProblem(Problem& p)
{
	//! ������ ��������� �� ���� ��������� ���������
	//! ������ ������ �������, ����� �������� �������������� ������: & -> |, | -> &
	for(unsigned i = 0; i < p.size(); ++i)
	{
		Clause& c = p[i];
		for(unsigned j = 0; j < c.size(); ++j)
		{
			c[j] ^= 1;
		}
	}
}

} // namespace SatLib
