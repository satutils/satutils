#include "../OutputFunctions.h"
#include <iostream>
#include <fstream>
#include <string>

namespace SatLib
{

void WriteBitVectorValues(const std::string& filename, const SatLib::BitVector& vec, 
			  unsigned var_id, const std::string& delim)
{
	std::ofstream out(filename.c_str(), std::ios::out);
	if(!out.is_open())
		throw std::runtime_error("can't open file " + filename);
	WriteBitVectorValues(out, vec, var_id, delim);
}

void WriteBitVectorValues(std::ostream& out, const SatLib::BitVector& vec, 
			  unsigned var_id, const std::string& delim)
{
	if(var_id > 0)
	{
		for(std::size_t i(0); i < vec.size(); ++i, ++var_id)
			out << (vec[i] & 1 ? "" : "-") << var_id << delim;
	}
	else
	{
		for(std::size_t i(0); i < vec.size(); ++i)
			out << (vec[i] & 1 ? "1" : "0") << delim;
		out << std::endl;
	}
}

} // namespace SatLib
