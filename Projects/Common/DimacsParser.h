#ifndef SAT_COMMON_DIMACS_PARSER_H
#define SAT_COMMON_DIMACS_PARSER_H

#include "Types.h"

namespace SatLib
{

template<typename IStream>
bool ParseDimacs(IStream& in, Problem& problem)
{
	for(;;)
	{
		SkipWhitespace(in);
		if(in.eof()) break;
		//! ������ ��������� DIMACS-�����
		else if((*in == 'p') && match(in, "p dnf"))
		{
			 int vars    = ParseInt(in);
             int clauses = ParseInt(in);
			 SkipLine(in);
		}
		//! ���������� �����������
		else if(*in == 'c')
			SkipLine(in);
		else
		{
			Clause cl;
			ReadClause(in, cl);
			if(cl.size())
				problem.push_back(cl);
		}
	}
	return true;
}

template<typename IStream>
bool ParseDimacs(IStream& in, Problem& problem, std::vector<bool>& is_xor)
{
	for(;;)
	{
		SkipWhitespace(in);
		if(in.eof()) break;
		//! ������ ��������� DIMACS-�����
		else if((*in == 'p') && match(in, "p dnf"))
		{
			 int vars    = ParseInt(in);
             int clauses = ParseInt(in);
			 SkipLine(in);
		}
		//! ���������� �����������
		else if(*in == 'c')
			SkipLine(in);
		else
		{
			bool is_xor_ = (*in == 'x');
			if(is_xor_) ++in;
			Clause cl;
			ReadClause(in, cl);
			if(cl.size())
			{
				problem.push_back(cl);
				// �������� ���� - �������� �� ��������� ����������� �������� ������ (true)
				is_xor.push_back(is_xor_);
			}
		}
	}
	return true;
}

template<typename IStream>
void SkipWhitespace(IStream& in)
{
	while ((*in >= 9 && *in <= 13) || *in == 32)
	{
        ++in; 
		if(in.eof()) return;
	}
}

template<typename IStream>
void SkipLine(IStream& in)
{
	while(in.eof() || *in != '\n') ++in;
	if(*in == '\n') ++in;
}

template<typename IStream>
bool match(IStream& in, const char* str)
{
	for (; *str != 0; ++str, ++in)
        if (*str != *in)
            return false;
    return true;
}

template<typename IStream>
int ParseInt(IStream& in)
{
	int     val = 0;
    bool    neg = false;
    SkipWhitespace(in);
    if      (*in == '-') neg = true, ++in;
    else if (*in == '+') ++in;
	if (*in < '0' || *in > '9') std::cout << "PARSE ERROR! Unexpected char:" << *in << std::endl, exit(1);
    while (*in >= '0' && *in <= '9')
        val = val * 10 + (*in - '0'),
        ++in;
    return neg ? -val : val;
}

template<typename IStream>
void ReadClause(IStream& in, Clause& cl)
{
	int var, lit;
	for(;;)
	{
		lit = ParseInt(in);
		if(lit == 0) break;
		var = abs(lit);
		cl.push_back( 2*var + ((lit > 0) ? 0 : 1) );
	}
}

} // namespace SatLib

#endif // SAT_COMMON_DIMACS_PARSER_H
