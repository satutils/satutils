﻿#ifndef SAT_COMMON_CHAR_STREAM_H
#define SAT_COMMON_CHAR_STREAM_H

#include <iostream>
#include "ICharStream.h"

namespace SatLib
{

class CharStream: public ICharStream
{
	enum { buffer_size = 64 * 1024 };
public:

	CharStream(std::istream& s = std::cin);

	virtual ~CharStream();

	virtual char operator * () const {return (pos_ < size_) ? buffer_[pos_] : EOF;}
	
	virtual void operator ++ () {pos_++; AssureLookahead();}
	
	virtual char operator[](int index) const { return (pos_ + index < size_ ? buffer_[pos_ + index] : EOF);}

	virtual unsigned  position() const {return pos_;}

	//! Проверяем, что и в буфере и в потоке больше нет данных
	virtual bool eof() const {return (pos_ >= size_) && in.eof();}

private:

	void AssureLookahead();

	std::istream& in;
	char* buffer_;
	//! позиция текущего символа в буфере
	unsigned pos_;
	//! число символов в буфере (size_ <= buffer_size)
	unsigned size_;
};

} // namespace SatLib

#endif // SAT_COMMON_CHAR_STREAM_H
