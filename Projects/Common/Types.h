#ifndef SAT_COMMON_TYPES_H
#define SAT_COMMON_TYPES_H

#include <vector>
#include <set>
#include <map>

namespace SatLib
{

typedef unsigned char      uint8_t;
typedef unsigned short int uint16_t;
typedef unsigned long int  uint32_t;
typedef unsigned long long uint64_t;

//! �������
typedef uint32_t Lit;
//! ����������� (��������/�������� � ����������� �� ���������)
typedef std::vector<Lit> Clause;
//! ������� (���/��� � ����������� �� ���������)
typedef std::vector<Clause> Problem;

typedef char Bit;
//! ������� ������������������ (������)
typedef std::vector<Bit> BitVector;

const Bit bFalse(0);
const Bit bTrue (1);

typedef unsigned char Byte;
typedef std::vector<Byte> ByteVector;

//! ������������� ���������� � �������
typedef unsigned int VarId;
typedef std::set<VarId> VarSet;
typedef std::vector<VarId> VarContainter;
typedef std::pair<VarId, unsigned> VarCounter;
typedef std::map<VarId, unsigned> VarCounterMap;

//! ������������ VarId (����� ���������� - ������) <--> unsigned (�������)
typedef std::vector<unsigned> VarOrder;

} // namespace SatLib

#endif // SAT_COMMON_TYPES_H
