#ifndef SAT_COMMON_RND_GEN_H
#define SAT_COMMON_RND_GEN_H

#include "Types.h"
#include <fstream>
#include <string>

namespace SatLib
{

bool GetRandomBit();

uint8_t GetRandomByte();

template <typename T>
T GetRandom()
{
	T x = 0;
	for(std::size_t i = 0; i < sizeof(T); ++i)
		x |= static_cast<T>(GetRandomByte()) << i * sizeof(uint8_t);
	return x;
}

//! ������ ��������� ������������������ ��������� �����.
//! ��������� ������ �������� �� ���������� �����.
class GenTrueRandom
{
public:

	GenTrueRandom(const std::string& filename);

	//! ������ size ��������� ��� 
	bool GetRandomStream(std::size_t size, std::vector<char>& stream);
	//! ������ size ��������� ����
	bool GetRandomBytes(std::size_t size, std::vector<uint8_t>& stream);

private:

	//! ����, ���������� ��������� ������
	std::ifstream file_;
	//! ������ ����� � ������
	std::size_t file_size_;
};

} // namespace SatLib

#endif // SAT_COMMON_RND_GEN_H
