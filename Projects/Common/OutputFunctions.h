#ifndef SAT_COMMON_OUTPUT_FUNCTIONS_H
#define SAT_COMMON_OUTPUT_FUNCTIONS_H

#include "Types.h"

namespace SatLib
{

template <typename T>
void WriteBits(std::ostream& out, const T& data)
{
	for(std::size_t i(0), cnt = 8 * sizeof(T); i < cnt; ++i)
		out << (b & (1<<i) ? "1" : "0");
}

template <typename Vec>
void WriteBitVector(std::ostream& out, const Vec& vec)
{
	for(std::size_t i(0); i < vec.size(); ++i)
		out << (vec[i] ? "1" : "0");
}

template <typename Vec>
void WriteByteVector(std::ostream& out, const Vec& vec)
{
	for(std::size_t i(0); i < vec.size(); ++i)
	{
		const Byte byte = static_cast<Byte>(vec[i]);
		for(std::size_t j(0); j < 8; ++j)
			out << (byte & (1 << j) ? "1" : "0");
	}
}

void WriteBitVectorValues(const std::string& filename, const SatLib::BitVector& vec, 
			  unsigned var_id, const std::string& delim = " ");

void WriteBitVectorValues(std::ostream& out, const SatLib::BitVector& vec, 
			  unsigned var_id, const std::string& delim = " ");

} // namespace SatLib

#endif // SAT_COMMON_OUTPUT_FUNCTIONS_H
