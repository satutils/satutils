#ifndef SAT_COMMON_UTILS_H
#define SAT_COMMON_UTILS_H

#include "Types.h"
#include <ctime>

namespace SatLib
{

//! ����� �� ������ 2 ���� ��� �����
template <typename T>
T Parity(T val)
{
	for(std::size_t shift = 4 * sizeof(T); shift > 0; shift /= 2)
	{
		val ^= val >> shift;
	}
	return val & 1;
}

inline double cpuTime(void) { return (double)clock() / CLOCKS_PER_SEC; }

//! ������� ��������� ���������� ����������
void ExtractVarSet(const Problem& problem, VarSet& var_set);

void ExtractVarCounterMap(const Problem& problem, VarCounterMap& var_map);

//! ����������� � ������������ ����� ����������
void GetMinMaxVars(const Problem& problem, VarId& min_var_id, VarId& max_var_id);

//! ��������� ���� � ����� � ��� �����
void SplitFilename(const std::string& path, std::string& dir, std::string& filename);

//! ��������� ��� ����� �� ��� � �������
void SplitFileSuffix(const std::string& filename, std::string& name, std::string& suffix);

//! ��������� ������������� �����
bool FileExists(const std::string& filename);

//! ����� ��������� � �������
std::size_t GetLiteralCount(const Problem& problem);

//! ������ ������� � ������
std::size_t GetProblemBinarySize(const Problem& problem);

//! ������ ���������� ������ ��� �������
std::size_t GetProblemMemoryAlloc(const Problem& problem);

void FillVarOrder(const Problem& problem, VarOrder& order, const std::string& order_type);

//ProblemType GetProblemType(const std::string& str_type);

//! ��������� �������. ����������� ��� �������� �� ��� ����������� ��� � ���.
void NegateProblem(Problem& problem);

} // namespace SatLib

#endif // SAT_COMMON_UTILS_H
