#ifndef SAT_COMMON_ICHAR_STREAM_H
#define SAT_COMMON_ICHAR_STREAM_H

namespace SatLib
{

class ICharStream
{
public:
	virtual ~ICharStream() {}

	virtual char operator* () const = 0;
	
	virtual void operator++ () = 0;
	
	virtual char operator[](int offset) const = 0;

	virtual unsigned position() const = 0;

	//! ���������, ��� � � ������ � � ������ ������ ��� ������
	virtual bool eof() const = 0;
};

inline bool isEof(ICharStream& in) { return *in == EOF; }

} // namespace SatLib

#endif // SAT_COMMON_ICHAR_STREAM_H
