#ifndef CIPHERLIB_ISTREAM_CIPHER_H
#define CIPHERLIB_ISTREAM_CIPHER_H

#include "Common/Types.h"

namespace CipherLib
{

class IStreamCipher 
{
public:

	virtual ~IStreamCipher() {}
	//! ������������� ��������� ����������
	virtual void init() = 0;
	//! ����� ��������� ����������
	virtual void reset() = 0;
	//! ��������� �����
	virtual void setKey(const SatLib::BitVector& key) = 0;
	//! ��������� ����������������� �������
	virtual void setIV(const SatLib::BitVector& iv) = 0;
	//! ��������� ���������� ���� ��������� ������
	virtual SatLib::Bit getNextBit() = 0;
	//! ��������� �������� ������� �������� ������
	virtual void getStreamBit(SatLib::BitVector& vec, std::size_t size) = 0;
	//! �������� ������� ��������� ��������� ����������
	virtual void getRegisterState(SatLib::BitVector& vec) const = 0;
};

} // namespace CipherLib

#endif // CIPHERLIB_ISTREAM_CIPHER_H
