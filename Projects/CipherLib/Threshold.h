#ifndef CIPHERLIB_THRESHOLD_H
#define CIPHERLIB_THRESHOLD_H

#include "Common/Types.h"

namespace CipherLib
{

class Threshold
{
public:

	Threshold(): regA_(0), regB_(0), regC_(0), regD_(0), regE_(0) {}

	//! ����� ��������� ����������
	void reset();
	//! ��������� ����� (��������� ��������� ���������)
	void setKey(const std::vector<unsigned>& key);
	void setKey(unsigned key[], std::size_t key_size);
	//! ��������� ���������� ���� ��������� ������
	SatLib::Bit getNextBit();
	//! ��������� �������� ������� �������� ������
	void getStreamBit(SatLib::BitVector& vec, std::size_t size);
	//! �������� ������� ��������� ��������� ����������
	void getRegisterState(SatLib::BitVector& vec) const;

protected:

	unsigned ShiftRegister(unsigned& reg, unsigned mask, std::size_t reg_size);

private:
	//! ����� ��������� �������� �����
	static const unsigned regA_mask_ = 0x0684;
	static const unsigned regB_mask_ = 0x1182;
	static const unsigned regC_mask_ = 0x6802;
	static const unsigned regD_mask_ = 0xA084;
	static const unsigned regE_mask_ = 0x15E00;
	//! ������ ���������
	static const std::size_t regA_size_ = 11;
	static const std::size_t regB_size_ = 13;
	static const std::size_t regC_size_ = 15;
	static const std::size_t regD_size_ = 16;
	static const std::size_t regE_size_ = 17;
	//! ��������
	unsigned regA_, regB_, regC_, regD_, regE_;
};

} // namespace CipherLib

#endif // CIPHERLIB_THRESHOLD_H
