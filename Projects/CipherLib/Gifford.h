#ifndef CIPHERLIB_GIFFORD_H
#define CIPHERLIB_GIFFORD_H

#include "Common/Types.h"

namespace CipherLib
{

class Gifford
{
public:
	Gifford();
	//! ����� ��������� ����������
	void reset();
	//! ��������� �����
	void setKey(const SatLib::ByteVector& key);
	void setKey(SatLib::Byte key[], std::size_t key_size);
	//! ��������� ���������� ����� ��������� ������
	SatLib::Byte getNextByte();
	//! ��������� ������������������ ����
	void getStreamBit(SatLib::ByteVector& vec, std::size_t size);
	//! �������� ������� ��������� ��������� ����������
	void getRegisterState(SatLib::ByteVector& vec) const;

private:

	void shift_reg();

private:
	static const std::size_t reg_size_ = 8;
	SatLib::Byte reg_[reg_size_];
};

} // namespace CipherLib

#endif // CIPHERLIB_GIFFORD_H
