#ifndef CIPHERLIB_BIVIUM_H
#define CIPHERLIB_BIVIUM_H

#include "IStreamCipher.h"

namespace CipherLib
{

/*
* ������ ���������� ������������� ������ ��� ��������� �������� ������ - ��� (����, �������� �����).
* ����������� ���� ������������ � ������ ��� ��������� ������.
*/ 
class Bivium: public IStreamCipher
{
public:

	Bivium();
	//! ������������� ��������� ����������
	void init();
	//! ����� ��������� ����������
	void reset();
	//! ��������� �����
	void setKey(const SatLib::BitVector& key);
	//! ��������� ����������������� �������
	void setIV(const SatLib::BitVector& iv);
	//! ��������� ���������� ���� ��������� ������
	SatLib::Bit getNextBit();
	//! ��������� �������� ������� �������� ������
	void getStreamBit(SatLib::BitVector& vec, std::size_t size);
	//! �������� ������� ��������� ��������� ����������
	void getRegisterState(SatLib::BitVector& vec) const;

private:

	void shift_regs();

private:
	// ������ ������� ������� - 177 ������� �����
	static const std::size_t reg_size_ = 177;
	static const std::size_t lenA = 93;
	static const std::size_t lenB = 84;
	static const std::size_t key_size_ = 80;
	static const std::size_t iv_size_ = 80;

	SatLib::Bit m_reg[reg_size_];
	SatLib::Bit m_key[key_size_];
	SatLib::Bit m_iv[iv_size_];
};

} // namespace CipherLib

#endif // CIPHERLIB_BIVIUM_H
