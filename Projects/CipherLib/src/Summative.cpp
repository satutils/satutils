#include "stdafx.h"
#include "../Summative.h"
#include "Common/Utils.h"
#include "Common/Logger.h"

namespace CipherLib
{

void Summative::reset()
{
	regA_ = regB_ = regC_ = summator_ = 0;
}

unsigned Summative::ShiftRegister(unsigned& reg, unsigned mask, std::size_t reg_size)
{
	unsigned out = (reg >> (reg_size - 1)) & 1;
	reg = (reg << 1) | SatLib::Parity(reg & mask);
	return out;
}

void Summative::setKey(const std::vector<unsigned>& key)
{
	if(key.size() < 3)
	{
		LOG_WARNING("Summative::setKey(): key.size() < 3.");
	}
	regA_ = (key.size() > 0 ? key[0] : 0);
	regB_ = (key.size() > 1 ? key[1] : 0);
	regC_ = (key.size() > 2 ? key[2] : 0);
}

void Summative::setKey(unsigned key[], std::size_t key_size)
{
	if(key_size < 3)
	{
		LOG_WARNING("Summative::setKey(): key_size < 3.");
	}
	regA_ = (key_size > 0 ? key[0] : 0);
	regB_ = (key_size > 1 ? key[1] : 0);
	regC_ = (key_size > 2 ? key[2] : 0);
}

// fixme: ����� ��������� �������, �.�. �� �������� ��� ����������� �������
SatLib::Bit Summative::getNextBit()
{
	summator_ += 
		ShiftRegister(regA_, regA_mask_, regA_size_) + 
		ShiftRegister(regB_, regB_mask_, regB_size_) + 
		ShiftRegister(regC_, regC_mask_, regC_size_);

	const SatLib::Bit out_bit = static_cast<SatLib::Bit>(summator_ & 1);
	summator_ = (summator_ & 3) >> 1;
	return out_bit;
}

void Summative::getStreamBit(SatLib::BitVector& vec, std::size_t size)
{
	vec.resize(size);
	for(std::size_t i(0); i < size; ++i)
		vec[i] = getNextBit();
}

void Summative::getRegisterState(SatLib::BitVector& vec) const
{
	vec.resize(regA_size_ + regB_size_ + regC_size_);
	std::size_t ia(0), ib(0), ic(0);
	for(; ia < regA_size_; ++ia)
		vec[ia] = static_cast<SatLib::Bit>((regA_ >> ia) & 1);
	for(; ib < regB_size_; ++ib)
		vec[regA_size_ + ib] = static_cast<SatLib::Bit>((regB_ >> ib) & 1);
	for(; ic < regC_size_; ++ic)
		vec[regA_size_ + regB_size_ + ic] = static_cast<SatLib::Bit>((regC_ >> ic) & 1);
}

} // namespace CipherLib
