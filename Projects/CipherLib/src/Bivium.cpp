#include "stdafx.h"
#include "../Bivium.h"

namespace CipherLib
{

Bivium::Bivium()
{
	reset();
}

void Bivium::reset()
{
	memset(m_reg, 0, reg_size_);
	memset(m_key, 0, key_size_);
	memset(m_iv,  0, iv_size_);
}

void Bivium::setKey(const SatLib::BitVector &key)
{
	memset(m_key, 0, key_size_);
	const std::size_t sz = std::min(key_size_, key.size());
	memcpy(m_key, &key[0], sz);
}

void Bivium::setIV(const SatLib::BitVector &iv)
{
	memset(m_iv, 0, iv_size_);
	const std::size_t sz = std::min(iv_size_, iv.size());
	memcpy(m_iv, &iv[0], sz);
}

void Bivium::init()
{
	//! ����� ������ �������� � ��������� ����� � ���������������� ������������������
	memset(m_reg, 0, reg_size_);
	memcpy(m_reg, m_key, key_size_);
	memcpy(m_reg + lenA, m_iv, iv_size_);
	m_reg[reg_size_ - 3] = 1;
	m_reg[reg_size_ - 2] = 1;
	m_reg[reg_size_ - 1] = 1;

	//! ������������� ����������
	for(std::size_t i(0), size(4 * reg_size_); i < size; ++i)
	{
		shift_regs();
	}
}

void Bivium::shift_regs()
{
	const SatLib::Bit t1 = m_reg[65]  ^ m_reg[90]  & m_reg[91]  ^ m_reg[92]  ^ m_reg[170];
	const SatLib::Bit t2 = m_reg[161] ^ m_reg[174] & m_reg[175] ^ m_reg[176] ^ m_reg[68];

	for(std::size_t i = lenA - 1; i > 0; i--)
		m_reg[i] = m_reg[i-1];
	m_reg[0] = t2;

	for(std::size_t i = lenB + lenA - 1; i > lenA; i--)
		m_reg[i] = m_reg[i-1];
	m_reg[lenA] = t1;
}

SatLib::Bit Bivium::getNextBit()
{
	const SatLib::Bit t1 = m_reg[65] ^ m_reg[92];
	const SatLib::Bit t2 = m_reg[161] ^ m_reg[176];
	shift_regs();
	return t1 ^ t2;
}

void Bivium::getStreamBit(SatLib::BitVector& vec, std::size_t size)
{
	vec.resize(size);
	for(std::size_t i(0); i < size; ++i)
		vec[i] = getNextBit();
}

void Bivium::getRegisterState(SatLib::BitVector& vec) const
{
	vec.resize(reg_size_);
	memcpy(&vec[0], m_reg, reg_size_);
}

} // namespace CipherLib
