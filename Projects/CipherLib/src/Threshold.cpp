#include "stdafx.h"
#include "../Threshold.h"
#include "Common/Utils.h"
#include "Common/Logger.h"

namespace CipherLib
{

void Threshold::reset()
{
	regA_ = regB_ = regC_ = regD_ = regE_ = 0;
}

unsigned Threshold::ShiftRegister(unsigned& reg, unsigned mask, std::size_t reg_size)
{
	unsigned out = (reg >> (reg_size - 1)) & 1;
	reg = (reg << 1) | SatLib::Parity(reg & mask);
	return out;
}

void Threshold::setKey(const std::vector<unsigned>& key)
{
	if(key.size() < 5)
	{
		LOG_WARNING("Threshold::setKey(): key.size() < 5.");
	}
	regA_ = (key.size() > 0 ? key[0] : 0);
	regB_ = (key.size() > 1 ? key[1] : 0);
	regC_ = (key.size() > 2 ? key[2] : 0);
	regD_ = (key.size() > 3 ? key[3] : 0);
	regE_ = (key.size() > 4 ? key[4] : 0);
}

void Threshold::setKey(unsigned key[], std::size_t key_size)
{
	if(key_size < 5)
	{
		LOG_WARNING("Threshold::setKey(): key_size < 5.");
	}
	regA_ = (key_size > 0 ? key[0] : 0);
	regB_ = (key_size > 1 ? key[1] : 0);
	regC_ = (key_size > 2 ? key[2] : 0);
	regD_ = (key_size > 3 ? key[3] : 0);
	regE_ = (key_size > 4 ? key[4] : 0);
}

SatLib::Bit Threshold::getNextBit()
{
	const unsigned sum = 
		ShiftRegister(regA_, regA_mask_, regA_size_) + 
		ShiftRegister(regB_, regB_mask_, regB_size_) + 
		ShiftRegister(regC_, regC_mask_, regC_size_) + 
		ShiftRegister(regD_, regD_mask_, regD_size_) + 
		ShiftRegister(regE_, regE_mask_, regE_size_);
	//! ���� ������ �������� ��������� ������ 1, �� �������� �������� ��� 1. ����� 0.
	return (sum > 2 ? SatLib::bTrue : SatLib::bFalse);
}

void Threshold::getStreamBit(SatLib::BitVector& vec, std::size_t size)
{
	vec.resize(size);
	for(std::size_t i(0); i < size; ++i)
		vec[i] = getNextBit();
}

void Threshold::getRegisterState(SatLib::BitVector& vec) const
{
	vec.resize(regA_size_ + regB_size_ + regC_size_ + regD_size_ + regE_size_);
	std::size_t i(0), offset(0);
	for(; i < regA_size_; ++i)
		vec[i] = static_cast<SatLib::Bit>((regA_ >> i) & 1);
	offset += regA_size_;
	for(i = 0; i < regB_size_; ++i)
		vec[offset + i] = static_cast<SatLib::Bit>((regB_ >> i) & 1);
	offset += regB_size_;
	for(i = 0; i < regC_size_; ++i)
		vec[offset + i] = static_cast<SatLib::Bit>((regC_ >> i) & 1);
	offset += regC_size_;
	for(i = 0; i < regD_size_; ++i)
		vec[offset + i] = static_cast<SatLib::Bit>((regD_ >> i) & 1);
	offset += regD_size_;
	for(i = 0; i < regE_size_; ++i)
		vec[offset + i] = static_cast<SatLib::Bit>((regE_ >> i) & 1);
}

} // namespace CipherLib
