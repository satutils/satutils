#include "stdafx.h"
#include "../Gifford.h"

namespace CipherLib
{

Gifford::Gifford()
{
	reset();
}

void Gifford::reset()
{
	memset(reg_, 0, reg_size_);
}

void Gifford::setKey(const SatLib::ByteVector& key)
{
	memset(reg_, 0, reg_size_);
	const std::size_t sz = std::min(reg_size_, key.size());
	memcpy(reg_, &key[0], sz);
}

void Gifford::setKey(SatLib::Byte key[], std::size_t key_size)
{
	memset(reg_, 0, reg_size_);
	const std::size_t sz = std::min(reg_size_, key_size);
	memcpy(reg_, key, sz);
}

SatLib::Byte Gifford::getNextByte()
{
	using namespace SatLib;
	//! ��������� ���� ��������� ������
	const uint32_t left = (static_cast<uint32_t>(reg_[2]) << 8) | static_cast<uint32_t>(reg_[0]);
	const uint32_t right = (static_cast<uint32_t>(reg_[7]) << 8) | static_cast<uint32_t>(reg_[4]);
	const uint32_t mult = left * right;
	SatLib::Byte result = static_cast<SatLib::Byte>((mult >> 16) & 0xFF);
	//! �������� ������� ����� ��� ��� ������� ���������
	shift_reg();
	return result;
}

void Gifford::shift_reg()
{
	//! ��������� ���� �������� �����
	SatLib::Byte feedback = (reg_[1] >> 1) | (reg_[1] & 0x80);
	feedback ^= reg_[0] ^ (reg_[7] << 1);
	for(std::size_t i = reg_size_ - 1; i > 0; i--)
		reg_[i] = reg_[i - 1];
	reg_[0] = feedback;
}

void Gifford::getStreamBit(SatLib::ByteVector& vec, std::size_t size)
{
	vec.resize(size);
	for(std::size_t i(0); i < size; ++i)
		vec[i] = getNextByte();
}

void Gifford::getRegisterState(SatLib::ByteVector& vec) const
{
	vec.resize(reg_size_);
	memcpy(&vec[0], reg_, reg_size_);
}

} // namespace CipherLib
