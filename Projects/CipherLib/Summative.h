#ifndef CIPHERLIB_SUMMATIVE_H
#define CIPHERLIB_SUMMATIVE_H

#include "Common/Types.h"

namespace CipherLib
{

class Summative
{
public:

	Summative(): regA_(0), regB_(0), regC_(0), summator_(0) {}

	//! ����� ��������� ����������
	void reset();
	//! ��������� ����� (��������� ��������� ���������)
	void setKey(const std::vector<unsigned>& key);
	void setKey(unsigned key[], std::size_t key_size);
	//! ��������� ���������� ���� ��������� ������
	SatLib::Bit getNextBit();
	//! ��������� �������� ������� �������� ������
	void getStreamBit(SatLib::BitVector& vec, std::size_t size);
	//! �������� ������� ��������� ��������� ����������
	void getRegisterState(SatLib::BitVector& vec) const;

protected:

	unsigned ShiftRegister(unsigned& reg, unsigned mask, std::size_t reg_size);

private:
	//! ����� ��������� �������� ����� (����� �� A5/1)
	static const unsigned regA_mask_ = 0x72000;
	static const unsigned regB_mask_ = 0x300000;
	static const unsigned regC_mask_ = 0x700080;
	//! ������ ���������
	static const std::size_t regA_size_ = 19;
	static const std::size_t regB_size_ = 22;
	static const std::size_t regC_size_ = 23;
	//! ��������
	unsigned regA_;
	unsigned regB_;
	unsigned regC_;
	//! �������� (���������� ��� ������� ����)
	unsigned summator_;
};

} // namespace CipherLib

#endif // CIPHERLIB_SUMMATIVE_H
